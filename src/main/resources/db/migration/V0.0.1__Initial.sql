CREATE TABLE products
(
    sku         VARCHAR(16)     NOT NULL
        CONSTRAINT pk_product_id PRIMARY KEY,
    name        VARCHAR(125)    NOT NULL,
    description VARCHAR(125),
    price       DECIMAL           NOT NULL,
    created_at  TIMESTAMP     NOT NULL,
    updated_at  TIMESTAMP     NOT NULL
);

CREATE TABLE stocks (
                       id INT PRIMARY KEY AUTO_INCREMENT,
                       product_sku VARCHAR(16),
                       quantity INT,
    -- Add other stock-related fields as needed
                       FOREIGN KEY (product_sku) REFERENCES products(sku)
);
