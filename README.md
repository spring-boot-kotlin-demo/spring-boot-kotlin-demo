# Dockerizing Your Spring Boot Kotlin Backend Application

Welcome to this guide on containerizing your Spring Boot Kotlin backend application with Docker. This step-by-step guide will help you package your application into a Docker container so you can easily deploy and run it across different environments.

## Prerequisites

Before you get started, make sure you have the following tools installed:

- [Docker](https://www.docker.com/get-started)

## Dockerfile

In this repository, you'll find a Dockerfile. This file contains the instructions needed to create a Docker image for your application. You don't need to edit this file unless you have specific requirements.

## Building the Docker Image

Follow these steps to create a Docker image for your Spring Boot Kotlin application:

1. Clone or download this repository to your computer.

2. Go to your Spring Boot Kotlin project's root directory.

3. Build your application using Gradle. Run this command:

`./gradlew build`

After this step, you'll have a JAR file in the `build/libs` directory.

4. Copy the Dockerfile from this repository into your project's root directory.

5. Open your terminal and navigate to your project's root directory.

6. Build the Docker image by running this command:

`docker build -t shop-spring-boot .`

## Running the Docker Container

Now that you've built the Docker image, you can run your Spring Boot Kotlin application in a Docker container. Follow these steps:

1. Start the Docker container using this command:

`docker run -d -p 8080:8080 shop-spring-boot`

This command launches a container based on your Docker image and maps port 8080 inside the container to your host machine's port 8080.

2. Access your Spring Boot application by opening a web browser and going to [http://localhost:8080](http://localhost:8080).
