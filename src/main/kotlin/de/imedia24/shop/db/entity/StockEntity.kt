package de.imedia24.shop.db.entity

import java.math.BigInteger
import javax.persistence.*

@Entity
@Table(name = "stocks")
data class StockEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int?,
    @ManyToOne
    val product: ProductEntity,
    val quantity: Int,

)

data class ProductWithStock(val product: ProductEntity, val stock: StockEntity?)
