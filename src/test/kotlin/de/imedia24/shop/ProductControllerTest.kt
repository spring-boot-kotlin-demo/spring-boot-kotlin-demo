import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.ProductWithStock
import de.imedia24.shop.db.entity.StockEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.math.BigDecimal

class ProductControllerTest {

    @Mock
    private lateinit var productService: ProductService

    private lateinit var productController: ProductController

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        productController = ProductController(productService)

    }

    @Test
    fun `getProductBySku should return 200 OK and product information when product exists`() {
        // Arrange
        val sku = "ABC123"
        val product = ProductEntity(sku, "Product one", "description", BigDecimal(200))
        val stock = StockEntity(1, product, 10)
        val productWithStock = ProductWithStock(product, stock)
        Mockito.`when`(productService.findProductBySku(sku)).thenReturn(productWithStock)

        // Act
        val response: ResponseEntity<ProductWithStock> = productController.findProductsBySku(sku)

        // Assert
        assert(response.statusCode == HttpStatus.OK)
        assert(response.body == productWithStock)
    }

    @Test
    fun `getProductBySku should return 404 Not Found when product does not exist`() {
        // Arrange
        val sku = "NonExistentSKU"
        Mockito.`when`(productService.findProductBySku(sku)).thenReturn(null)

        // Act
        val response: ResponseEntity<ProductWithStock> = productController.findProductsBySku(sku)

        // Assert
        assert(response.statusCode == HttpStatus.NOT_FOUND)
        assert(response.body == null)
    }

    @Test
    fun testUpdateProductSuccess() {
        val sku = "SKU123"
        val name = "Updated Product"
        val description = "Updated Description"
        val price = BigDecimal("99.99")

        // Mock the behavior of the productService
        Mockito.`when`(productService.partiallyUpdateProduct(sku, name, description, price)).thenReturn(ProductResponse(sku, name, description, price))

        // Call the update endpoint
        val response: ResponseEntity<ProductResponse> = productController.update(sku, name, description, price)

        // Assert the response
        assert(response.statusCode == HttpStatus.OK)
        assert(response.body != null)
        assert(response.body!!.sku == sku)
        assert(response.body!!.name == name)
        assert(response.body!!.description == description)
        assert(response.body!!.price == price)
    }

    @Test
    fun testUpdateProductBadRequest() {
        val sku = "SKU123"
        val name = "Updated Product"
        val description = "Updated Description"
        val price = BigDecimal("99.99")

        // Mock the behavior of the productService to return null
        Mockito.`when`(productService.partiallyUpdateProduct(sku, name, description, price)).thenReturn(null)

        // Call the update endpoint
        val response: ResponseEntity<ProductResponse> = productController.update(sku, name, description, price)

        // Assert the response
        assert(response.statusCode == HttpStatus.BAD_REQUEST)
    }
}
