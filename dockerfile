# Use an OpenJDK base image with Java 11
FROM openjdk:11-jre-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the Spring Boot JAR file into the container
COPY build/libs/shop-0.0.1-SNAPSHOT.jar .

# Expose the port your Spring Boot application is running on (default is 8080)
EXPOSE 8080

# Define the command to run your Spring Boot application
CMD ["java", "-jar", "shop-0.0.1-SNAPSHOT.jar"]