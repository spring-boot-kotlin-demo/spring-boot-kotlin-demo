package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.ProductWithStock
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.db.repository.StockRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository, private val stockRepository: StockRepository) {

    fun findProductBySku(sku: String): ProductWithStock? {
        val product =  productRepository.findBySku(sku)
        if (product != null) {
            val stock = stockRepository.findByProduct(product)
            return ProductWithStock(product, stock)
        }
        return null

    }

    fun findProductsBySkus(skus: Array<String>) : Array<ProductEntity>? {
        return productRepository.findAllBySkuIn(skus)
    }

    fun getAllProducts(): MutableIterable<ProductEntity>? {
        return productRepository.findAll()
    }

    fun createProduct(product : ProductEntity): ProductResponse? {
        val productObject =  ProductEntity(product.sku, product.name, product.description, product.price, product.createdAt)
        return productRepository.save(productObject).toProductResponse()
    }

    fun partiallyUpdateProduct(sku : String, name: String, description : String, price : BigDecimal) : ProductResponse? {
        val productObject = findProductBySku(sku)
        val product = ProductEntity(sku, name, description, price)
        return productRepository.save(product).toProductResponse()
    }
}
