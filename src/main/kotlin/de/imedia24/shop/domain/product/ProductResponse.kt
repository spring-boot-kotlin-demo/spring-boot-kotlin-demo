package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductResponse(
    val sku: String,
    var name: String,
    var description: String,
    var price: BigDecimal,
    val createdAt : ZonedDateTime = ZonedDateTime.now(),
    val updatedAt : ZonedDateTime = ZonedDateTime.now(),
) {
    companion object {
        fun ProductEntity.toProductResponse() = ProductResponse(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price,
            createdAt = createdAt,
            updatedAt = updatedAt
        )
    }
}
