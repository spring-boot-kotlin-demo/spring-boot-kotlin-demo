package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.ProductWithStock
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.ZonedDateTime

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!
    
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductWithStock> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.status(200).body(product)
        }
    }

    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun createProduct( @RequestBody @RequestParam sku: String, @RequestParam name: String, @RequestParam description: String, @RequestParam price: BigDecimal) : ResponseEntity<ProductResponse> {
        logger.info("creating product")
        val product = ProductEntity(sku = sku, name = name, description = description, price = price,  createdAt = ZonedDateTime.now(), updatedAt = ZonedDateTime.now())
        val result = productService.createProduct(product);
        return if(result == null){
            ResponseEntity.badRequest().build();
        }else {
            ResponseEntity.ok().body(result)
        }
    }

    @PutMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun update(
        @PathVariable("sku") sku: String,
        @RequestBody @RequestParam name: String, @RequestParam description: String, @RequestParam price: BigDecimal
    ): ResponseEntity<ProductResponse> {
        logger.info("update product : $sku")
        val result = productService.partiallyUpdateProduct(sku, name, description, price);
        return if(result == null){
            ResponseEntity.badRequest().build();
        }else {
            ResponseEntity.ok().body(result)
        }
    }

    @GetMapping("/products/All", produces = ["application/json;charset=utf-8"])
    fun getProducts(): ResponseEntity<MutableIterable<ProductEntity>> {
        val result =  productService.getAllProducts()
        return if(result == null){
            ResponseEntity.badRequest().build();
        }else {
            ResponseEntity.ok().body(result)
        }
    }

    @GetMapping("/products" , produces = ["application/json;charset=utf-8"])
    fun getProductsBySkus(
        @RequestParam("skus") skus: String
    ): ResponseEntity<Array<ProductEntity>> {
        logger.info("retrieving products in : $skus")
        val skusArrayFromPath = skus.split(",").toTypedArray()
        val result = productService.findProductsBySkus(skusArrayFromPath)
        return if(result == null){
            ResponseEntity.badRequest().build();
        }else {
            ResponseEntity.ok().body(result)
        }
    }

}
